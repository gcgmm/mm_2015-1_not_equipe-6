﻿using UnityEngine;
using System.Collections;

public class InvertVirtualCam : MonoBehaviour {

	Camera camera;
	public Vector3 v;

	void Start() {
		camera = GetComponent<Camera>();
	}

	void OnPreCull () {

		camera.ResetWorldToCameraMatrix ();
		camera.ResetProjectionMatrix ();

		Matrix4x4 p = camera.projectionMatrix;
		p = p * Matrix4x4.Scale(v);

		camera.projectionMatrix = p;

	}
	
	void OnPreRender () {
		GL.invertCulling = true;
	}
	
	void OnPostRender () {
		GL.invertCulling = false;
	}
}
