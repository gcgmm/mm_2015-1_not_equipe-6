﻿using UnityEngine;
using System.Collections;

public class Command : MonoBehaviour {
	
	public int cows;
	public int dogs;
	public int fishes;
	public int mice;
	public int pigs;

	public bool CheckCommand(int c, int d,int f, int m, int p)
	{
		return (cows == c) && (dogs == d) && (fishes == f) && (mice == m) && (pigs == p);
	}

	public string GetCommandText()
	{
		string retorno = "";

		if (cows == 1)
			retorno += "1 Vaca ";
		else if (cows > 1)
			retorno += cows + " Vacas ";

		if (dogs == 1)
			retorno += "1 Cachorro ";
		else if (dogs > 1)
			retorno += dogs + " Cachorros ";

		if (fishes == 1)
			retorno += "1 Peixe ";
		else if (fishes > 1)
			retorno += fishes + " Peixes ";
		
		if (mice == 1)
			retorno += "1 Rato ";
		else if (mice > 1)
			retorno += mice + " Ratos ";
		
		if (pigs == 1)
			retorno += "1 Porco";
		else if (pigs > 1)
			retorno += pigs + " Porcos";

		return retorno;
	}

}
